﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace TypeAccess
{
    class Program
    {
        static void Main(string[] args)
        {
            Repo repo = new Repo();
            Repo2 repo2 = new Repo2();
            Setup(repo);

            //Foo(repo);
            //AchStuff(repo);
            //PipelineFun(repo);
            SetupRaw(repo2);
            Raw(repo2);



        }



        /*
         * Imporovments:
         *  Split handler to smaller interfaces
         *  Accessor is now told what properties to use
         *  Decoupled from property attributes
         *  No need for an ordering integer, order is implied
         */

        // this seperatation supports seperation of concerns. 
        public static void Setup(Repo repo)
        {
            // concept: offset within delimited context.

            repo.Register<Test>()
                .Property(r => r.MyInteger1)
                //.Property(r => r.MyInteger1, new NumberAccess(), new LengthManagment(maxLength: 3, truncate: FieldTruncate.Right), new AlignmentManagment(maxLength: 10, align: FieldAlignment.Left))
                //.Property(r => r.MyInteger1, new NumberAccess(), o =>
                //{
                //    o.Maximum(3, FieldTruncate.Right);      //  optional (custom scenario)
                //    o.Minimum(10, FieldAlignment.Left);     // optional (custom scenario)
                //})
                .Property(r => r.MyInteger2, 2, new NumberAccess(), o =>
                {
                    o.Maximum(FieldTruncate.Right); // required, default to none
                    //o.Minimum(FieldAlignment.Left); // optional, defaul to Left
                })
                .Property(r => r.MyDateTime, new DateTimeAccess(), new AlignmentManagment(maxLength: 30, align: FieldAlign.Right))
                .Property(r => r.Complex, new ComplexAccess4())
                .Property(r => r.MyString, new StringAccess())
                .Property(r => r.MyByte, new NumberAccess(format: "X2", scale: 2));
            //.Property(r => r.MyDateTime)
            //.Property(r => r.MyInteger2);
            //.Property(r => r.Complex.X, new Int32Access4("c"));    //todo: how can we handle this?

            var fmt = new NumberAccess(scale: 100);

            repo.Register<Complex>()
                .Property(r => r.X, fmt)
                .Property(r => r.Y, fmt);


            // ACH
            //repo.RegisterAttributes<EntryDetailRecord>()
            //    .Go();

            // IBIP
            //repo.RegisterAttributes<DetailRecord>()
            //    .Go();

            //repo.RegisterAttributes<TrailerRecord>()
            //    .GoFixed();

            //Config c = new Config(repo);

            //c.Fixed<TrailerRecord>("\r\n")
            //    //.Property(r => r.RecordType, 0, 2, new StringAccess(), new LengthManagment(2, FieldTruncate.Right))
            //    .Property(r => r.RecordType, 0, 2, new StringAccess(), o =>
            //    {
            //        o.Maximum(FieldTruncate.Right);     //required
            //        o.Minimum(FieldAlignment.Right);    //required
            //    })
            //    .Property(r => r.ManufacturerCode, 2, 2, new NumberAccess(format: "00"))
            //    .Property(r => r.FileDate, 4, 8, new DateTimeAccess(format: "yyyyMMdd"))
            //    .Property(r => r.TotalDetailRecords, 12, 11, new NumberAccess(format: "00000000000"))
            //    .Property(r => r.Filler, 23, 577, new StringAccess());
        }



        public static void SetupRaw(Repo2 repo)
        {

            Package package1 = new Package
            {
                Writer = new RecordWriter(null, fieldDelimiter: "*"),
                Reader = new RecordReader(null, fieldDelimiter: "*"),
                Predicate = s => s == "A",
                Create = () => new Test(),
                Descriptions = new List<IDescription2>
                {
                    new Description3()
                    {
                        Nugget = new Nugget(GetProperty<Test, string>(r => r.RecordType), new StringAccess()),
                        Field = new Field(0)
                    },
                    new Description3()
                    {
                        Nugget = new Nugget(GetProperty<Test, int>(r => r.MyInteger1), new NumberAccess()),
                        Field = new Field(1)
                    },
                    new Description3()
                    {
                        Nugget = new Nugget(GetProperty<Test, DateTime>(r => r.MyDateTime), new DateTimeAccess()),
                        Field = new Field(3)
                    }
                }
            };

            repo.Add<Test>(package1);

            Package package2 = new Package
            {
                Writer = new RecordWriterFixed(null),
                Reader = new RecordReaderFixed(null),
                Predicate = s => s == "B",
                Create = () => new Test2(),
                Descriptions = new List<IDescription2>
                {
                    new Description3()
                    {
                        Nugget = new Nugget(GetProperty<Test2, string>(r => r.RecordType), new StringAccess()),
                        Field = new FixedLengthField(0, 3)
                    },
                    new Description3()
                    {
                        Nugget = new Nugget(GetProperty<Test2, int>(r => r.MyInteger1), new NumberAccess()),
                        Field = new FixedLengthField(3, 10)
                    },
                    new Description3()
                    {
                        Nugget = new Nugget(GetProperty<Test2, DateTime>(r => r.MyDateTime), new DateTimeAccess()),
                        Field = new FixedLengthField(15, 25)
                    }
                }
            };

            repo.Add<Test2>(package2);


            Package package3 = new Package
            {
                Writer = new RecordWriter(null),
                Reader = new RecordReader(null),
                Predicate = s => s == "C",
                Create = () => new Test3(),
                Descriptions = new List<IDescription2>
                {
                    new Description3()
                    {
                        Nugget = new Nugget(GetProperty<Test3, string>(r => r.RecordType), new StringAccess()),
                        Field = new Field(0)    //  ineresting how this is compatible with NamedFields. Maybe there should be an interface to indicate this.
                    },
                    new Description3()
                    {
                        Nugget = new Nugget(GetProperty<Test3, int>(r => r.MyInteger1), new NumberAccess()),
                        Field = new NamedField(1, "Integer1")
                    },
                    new Description3()
                    {
                        Nugget = new Nugget(GetProperty<Test3, DateTime>(r => r.MyDateTime), new DateTimeAccess()),
                        Field = new NamedField(2, "Date")
                    }
                }
            };

            repo.Add<Test3>(package3);


            Package package4 = new Package
            {
                Writer = new RecordWriterFixed(null),
                Reader = new RecordReaderFixed(null),
                Descriptions = new List<IDescription2>
                {
                    new Description3()
                    {
                        Nugget = new Nugget(GetProperty<Complex, int>(r => r.X), new NumberAccess(format: "000")),
                        Field = new FixedLengthField(3, 10)
                    },
                    new Description3()
                    {
                        Nugget = new Nugget(GetProperty<Complex, int>(r => r.Y), new NumberAccess(format: "000")),
                        Field = new FixedLengthField(15, 25)
                    }
                }
            };

            repo.Add<Complex>(package4);
        }


        public static void Raw(Repo2 repo)
        {
            Master master = new Master(repo);

            Test test1 = new Test { MyInteger1 = 12345, MyDateTime = DateTime.Now };

            RunRaw(master, test1);
            RunMany(master, test1);

            Test2 testA = new Test2 { MyInteger1 = 12345, MyDateTime = DateTime.Now };

            RunRaw(master, testA);

            Test3 testE = new Test3 { MyInteger1 = 12345, MyDateTime = DateTime.Now };

            RunRaw(master, testE);
            RunRawNamed<Test3>(master);

            Complex testC = new Complex { X = 15, Y = 97 };

            RunRaw(master, testC);

            RunManyBase<TestBase>(master, test1, testA, testE);
            RunManyMixed(master, test1, testA, testE);//, testC);

        }

        public static void RunRaw<T>(Master master, T test1) where T : new()
        {
            StringBuilder sb = new StringBuilder();
            TextWriter tw = new StringWriter(sb);

            master.Write(test1, tw);

            string result = sb.ToString();


            TextReader tr = new StringReader(result);

            T test2 = new T();
            master.Read(test2, tr);

            tr = new StringReader(result);

            T test3 = master.Read<T>(tr);
        }

        public static void RunRawNamed<T>(Master master) where T : new()
        {
            string result = "C,Date=04/11/1976,Integer2=4567,Integer1=1234\r\n";

            TextReader tr = new StringReader(result);

            T test2 = new T();
            master.Read(test2, tr);
        }

        // Every item is exactly the same T
        public static void RunMany<T>(Master master, T test1) where T : new()
        {
            StringBuilder sb = new StringBuilder();
            TextWriter tw = new StringWriter(sb);

            master.WriteMany(Enumerable.Repeat(test1, 5), tw);

            string result = sb.ToString();

            TextReader tr = new StringReader(result);
            IEnumerable<T> test3 = master.ReadMany<T>(tr).ToArray();
        }

        // Every item derrives from T
        public static void RunManyBase<T>(Master master, params T[] records) where T : new()
        {
            StringBuilder sb = new StringBuilder();
            TextWriter tw = new StringWriter(sb);

            master.WriteMany(records, tw);

            string result = sb.ToString();

            TextReader tr = new StringReader(result);
            IEnumerable<T> test3 = master.ReadMany<T>(tr).ToArray();
        }
        
        

        public static void RunManyMixed(Master master, params object[] records)
        {
            StringBuilder sb = new StringBuilder();
            TextWriter tw = new StringWriter(sb);

            master.WriteMany(records, tw);

            string result = sb.ToString();


            TextReader tr = new StringReader(result);
            IEnumerable test3 = master.ReadMany(tr).OfType<object>().ToArray();

            
        }


        public static void PipelineFun(Repo repo)
        {
            PipelineManager pipeline = new PipelineManager(repo.Get<Test>());

            var complex = new Complex { X = 1, Y = 2 };
            var test1 = new Test { MyInteger1 = 1234, MyInteger2 = 5678, MyDateTime = DateTime.Now, Complex = complex, MyString = "ABC", MyByte = (byte)0x3 };


            var sb1 = new StringBuilder();
            var writer1 = new RecordWriter(new StringWriter(sb1));

            pipeline.GetString(test1, writer1);

            var a = sb1.ToString();



            var reader1 = new RecordReader(new StringReader(sb1.ToString()));

            var test1a = new Test();
            pipeline.GetObject(test1a, reader1);


            ////////////////////////////////////



            //pipeline = new PipelineManager(repo.Get<TrailerRecord>());

            //var test2 = new TrailerRecord { RecordType = "TRJJJ", ManufacturerCode = 7, FileDate = DateTime.Now, TotalDetailRecords = 33, Filler = null };

            //var sb2 = new StringBuilder();
            //var writer2 = new RecordWriterFixed(new StringWriter(sb2));

            //pipeline.GetString(test2, writer2);

            //var b = sb2.ToString();


            //var reader2 = new RecordReaderFixed(new StringReader(sb2.ToString()));

            //var test2b = new TrailerRecord();
            //pipeline.GetObject(test2b, reader2);

        }


        private static PropertyInfo GetProperty<T, R>(Expression<Func<T, R>> propertyExpression)
        {
            var memberExpression = propertyExpression.Body as MemberExpression;

            return memberExpression.Member as PropertyInfo;
        }

        //public static void Foo(Repo repo)
        //{
        //    //var ra = repo.GetAccess<Test>();

        //    //RecordAccess ra = new RecordAccess();
        //    //ra.Load(builder._descriptions);
        //    //var stringify = ra.CreateStringifyMethod<object>();

        //    var complex = new Complex { X = 1, Y = 2 };
        //    var test1 = new Test { MyInteger1 = 1234, MyInteger2 = 5678, MyDateTime = DateTime.Now, Complex = complex, MyString = "ABC", MyByte = (byte)0x3 };
        //    var test2 = new Test { MyInteger1 = 9999, MyInteger2 = 1111, MyDateTime = DateTime.Now, Complex = complex, MyString = "ABC", MyByte = (byte)0x5 };

        //    var a = repo.Stringify(test1);
        //    var b = repo.Stringify(test2);

        //    //var a = stringify(test1);
        //    //var b = stringify(test2);


        //    //var objectify = ra.CreateObjectifyMethod2<object>();

        //    var test1a = new Test();
        //    repo.Objectify(test1a, a);
        //    //objectify(a, test1);


        //    var aa = repo.Stringify(complex);

        //    var test3aa = new Complex();
        //    repo.Objectify(test3aa, aa);
        //}

        //public static void AchStuff(Repo repo)
        //{
        //    EntryDetailRecord r = new EntryDetailRecord
        //    {
        //        RecordTypeCode = 6,
        //        TransactionCode = 1,
        //        TransitRoutingNumber = 1234
        //    };

        //    var a = repo.Stringify(r);     // writer would take an instance of Repo. Writer would have record formatting info in addition.

        //    var ra = new EntryDetailRecord();
        //    repo.Objectify(ra, a);
        //}
    }













    public class TestBase
    {
        public string RecordType { get; set; }
    }

    public class Test : TestBase
    {        
        public int MyInteger1 { get; set; }
        public int MyInteger2 { get; set; }
        public DateTime MyDateTime { get; set; }
        public Complex Complex { get; set; }

        public string MyString { get; set; }

        public byte MyByte { get; set; }

        public Test()
        {
            RecordType = "A";
        }
    }

    public class Test2 : Test
    {
        public Test2()
        {
            RecordType = "B";
        }
    }

    public class Test3 : Test
    {
        public Test3()
        {
            RecordType = "C";
        }
    }

    public class Complex
    {
        public int X { get; set; }
        public int Y { get; set; }
    }

    public class ComplexAccess4 : IPropertyAccess<Complex>
    {
        public string GetString(Complex value)
        {
            return String.Format("{0}-{1}", value.X, value.Y);     // todo: how does comma effect record format?
        }

        public Complex GetValue(string value)
        {
            string[] parts = value.Split('-');
            return new Complex
            {
                X = Int32.Parse(parts[0]),
                Y = Int32.Parse(parts[1])
            };
        }
    }

    public class GenericAccess4<T> : IPropertyAccess<T>
    {
        public string GetString(T value)
        {
            return value.ToString();
        }

        public T GetValue(string value) //todo: what else can we do?
        {
            return default(T);
        }
    }
}
