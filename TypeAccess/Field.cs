﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TypeAccess
{
    public interface IField
    {
        void Write(IRecordWriter writer, string value);
        string Read(IRecordReader reader);
    }

    // Fixed-length
    public class FixedLengthField : IField
    {
        private readonly int _index;
        private readonly int _length;
        private readonly IMaximumLengthBehavior _maximumLength;
        private readonly IMinimumLengthBehavior _minimumLength;

        public int Index
        {
            get { return _index; }
        }

        public int Length
        {
            get { return _index; }
        }

        public FixedLengthField(int index, int length, FieldTruncate truncate = FieldTruncate.None, FieldAlign align = FieldAlign.Left)
        {
            _index = index;
            _length = length;
            _maximumLength = new MaximumLengthBehavior(length, truncate);
            _minimumLength = new MinimumLengthBehavior(length, align);
        }

        public FixedLengthField(int index)
        {
            _index = index;
            _length = -1;
        }

        public void Write(IRecordWriter writer, string value)
        {
            if (_maximumLength != null)
                value = _maximumLength.Write(value);

            if (_minimumLength != null)
                value = _minimumLength.Write(value);

            writer.Write(value, 0, _index);
        }

        public string Read(IRecordReader reader)
        {
            string value = reader.Fetch(0, _index, _length);

            if (_minimumLength != null)
                value = _minimumLength.Read(value);

            return value;
        }
    }

    public class Field : IField
    {
        private readonly int _position;
        private readonly int _length;
        private readonly IMaximumLengthBehavior _maximumLength;
        private readonly IMinimumLengthBehavior _minimumLength;

        public int Position
        {
            get { return _position; }
        }

        public int Length
        {
            get { return _length; }
        }

        // length
        public Field(int position, int length, FieldTruncate truncate = FieldTruncate.None, FieldAlign align = FieldAlign.Left)
        {
            _position = position;
            _length = length;
            _maximumLength = new MaximumLengthBehavior(length, truncate);
            _minimumLength = new MinimumLengthBehavior(length, align);
        }

        // max length
        public Field(int position, int length, FieldTruncate truncate = FieldTruncate.None)
        {
            _position = position;
            _length = length;
            _maximumLength = new MaximumLengthBehavior(length, truncate);
        }

        // min length
        public Field(int position, int length, FieldAlign align = FieldAlign.Left)
        {
            _position = position;
            _length = length;
            _minimumLength = new MinimumLengthBehavior(length, align);
        }

        public Field(int position)
        {
            _position = position;
        }


        public void Write(IRecordWriter writer, string value)
        {
            if (_maximumLength != null)
                value = _maximumLength.Write(value);

            if (_minimumLength != null)
                value = _minimumLength.Write(value);

            writer.Write(value, _position, 0);
        }

        public string Read(IRecordReader reader)
        {
            string value = reader.Fetch(_position);

            if (_minimumLength != null)
                value = _minimumLength.Read(value);

            return value;
        }
    }


    // Experimiental
    public class NamedField : IField
    {
        private readonly int _position;
        private readonly string _name;        

        public int Position
        {
            get { return _position; }
        }

        public string Name
        {
            get { return _name; }
        }

        public NamedField(int position, string name)
        {
            _position = position;
            _name = name;
        }
        
        public void Write(IRecordWriter writer, string value)
        {
            value = String.Format("{0}={1}", _name, value); // is this a behavior?  It could tell us how to write the value and how to match & parse on read

            writer.Write(value, _position);
        }

        public string Read(IRecordReader reader)
        {
            // assume that the value for this field can be at any position
            
            for(int i = 0; i < reader.PartCount; i++)       // maybe we expose parts as IEnumerable?. We need to know i and the value
            {
                string value = reader.Fetch(i);

                if (value.StartsWith(_name))
                    return value.Substring(value.IndexOf('=') + 1);
            }

            return null;
        }
    }
}
