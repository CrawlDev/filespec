﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TypeAccess
{
    public class DateTimeAccess : IPropertyAccess<DateTime>
    {
        private string _format;

        public DateTimeAccess(string format = null)
        {
            _format = format;
        }

        public string GetString(DateTime value)
        {
            return value.ToString(_format);
        }

        public DateTime GetValue(string value)
        {
            if (_format != null)
                return DateTime.ParseExact(value, _format, null);

            return DateTime.Parse(value);
        }
    }
}
