﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TypeAccess
{
    public enum FieldTruncate
    {
        None,
        Left,
        Right
    }

    public enum FieldAlign
    {
        Left,
        Right
    }
}
