﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace TypeAccess
{
    public interface IDescription
    {
        PropertyInfo Property { get; }
        object Access { get; }
        IHandler[] Handlers { get; set; }   //todo: should this be here?

        void Write(IRecordWriter writer, string value);
        string Read(IRecordReader writer);
    }

    public class Description : IDescription
    {
        public int Position { get; set; }   //todo; this is for delimited and is missing an position property or a function to map from position in a record to this property
        public PropertyInfo Property { get; set; }
        public object Access { get; set; }
        public IHandler[] Handlers { get; set; }

        public void Write(IRecordWriter writer, string value)
        {
            if (Handlers != null)
            {
                foreach (IHandler handler in Handlers)
                    value = handler.Write(value);
            }

            writer.Write(value, Position, 0);
        }

        public string Read(IRecordReader reader)
        {
            string value = reader.Current()[Position];

            // steps
            if (Handlers != null)
            {
                foreach (IHandler handler in Handlers.Reverse())    //todo: probably dont want to use reverse
                    value = handler.Read(value);
            }

            return value;
        }
    }


    // we implement out logic without handlers because they arent simple inverses. Later we could add user handlers injected into the right place.
    // injectables could be like uppercase and transliterate, which would apply during write only.
    public class Description2 : IDescription
    {
        public int Index { get; set; }
        public int Length { get; set; }     // we rely on handlers to implement this. Length optional fo fixed length (not when reading? read to end?).

        public PropertyInfo Property { get; set; }
        public object Access { get; set; }
        public IHandler[] Handlers { get; set; }    // what if internalize this, to what we need to accomplish task at hand?


        public void Write(IRecordWriter writer, string value)
        {
            if (Handlers != null)
            {
                foreach (IHandler handler in Handlers)
                    value = handler.Write(value);
            }

            writer.Write(value, 0, Index);     // todo: we are not actually writing the value at the correct index.
        }

        public string Read(IRecordReader reader)
        {
            string value = reader.Current()[0].Substring(Index);    // this description only works with a non-delimiting reader. There should be a differnt interface

            if (Handlers != null)
            {
                foreach (IHandler handler in Handlers)//.Reverse())    //todo: probably dont want to use reverse
                    value = handler.Read(value);
            }

            return value;
        }
    }








    
}
