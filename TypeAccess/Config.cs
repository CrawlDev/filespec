﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace TypeAccess
{
    public class Config
    {
        private readonly Repo _repo;

        public Config(Repo repo)
        {
            _repo = repo;
        }

        public DelimitedConfig<T> Delimited<T>(string recordDelimiter, string fieldDelimiter)//, Action<DelimitedConfig<T>> action)
        {
            return new DelimitedConfig<T>(_repo);
        }

        public FixedConfig<T> Fixed<T>(string recordDelimiter)//, Action<DelimitedConfig<T>> action)
        {
            return new FixedConfig<T>(_repo);
        }

    }

    public class DelimitedConfig<T>
    {
        private readonly Repo _repo;

        public DelimitedConfig(Repo repo)
        {
            _repo = repo;
        }

        public DelimitedConfig<T> Property<R>(Expression<Func<T, R>> property, IPropertyAccess<R> access)
        {
            _repo.Add<T>(new Description { Position = _repo.CountDescriptions<T>(), Property = GetProperty(property), Access = access });

            return this;
        }

        private PropertyInfo GetProperty<R>(Expression<Func<T, R>> propertyExpression)
        {
            var memberExpression = propertyExpression.Body as MemberExpression;

            return memberExpression.Member as PropertyInfo;
        }
    }

    public class FixedConfig<T>
    {
        private readonly Repo _repo;

        public FixedConfig(Repo repo)
        {
            _repo = repo;
        }

        public FixedConfig<T> Property<R>(Expression<Func<T, R>> property, int index, int length, IPropertyAccess<R> access, params IHandler[] handlers)
        {
            List<IHandler> h = new List<IHandler>();    // todo: find a better way!


            // forcing this ensures that the value is a maximum length
            var len = handlers.OfType<LengthManagment>().SingleOrDefault();

            if (len == null)
                len = new LengthManagment(length, FieldTruncate.None);

            h.Add(len);

            // forcing this ensures that the value is a minimum length
            var align = handlers.OfType<AlignmentManagment>().SingleOrDefault();
            
            if (align == null)
                align = new AlignmentManagment(length, FieldAlign.Left);

            h.Add(align);
                  
               

            _repo.Add<T>(new Description2 { Index = index, Length = length, Property = GetProperty(property), Access = access, Handlers = h.ToArray() });

            return this;
        }

        public FixedConfig<T> Property<R>(Expression<Func<T, R>> property, int index, int length, IPropertyAccess<R> access, Action<SettingLengthKnownFixed> a)
        {
            //List<IHandler> h = new List<IHandler>();    // todo: find a better way!

            var settings = new SettingLengthKnownFixed(length);
            a(settings);

            //var len = new LengthManagment(length, settings.Truncate);
            //h.Add(len);

            //var align = new AlignmentManagment(length, settings.Align);
            //h.Add(align);

            var h = settings.GetHandlers().ToArray();

            _repo.Add<T>(new Description2 { Index = index, Length = length, Property = GetProperty(property), Access = access, Handlers = h.ToArray() });

            return this;
        }

        private PropertyInfo GetProperty<R>(Expression<Func<T, R>> propertyExpression)
        {
            var memberExpression = propertyExpression.Body as MemberExpression;

            return memberExpression.Member as PropertyInfo;
        }
    }

    public class FixedSettings
    {
        public FieldTruncate Truncate { get; set; }
        public FieldAlign Align { get; set; }

        public FixedSettings()
        {
            Truncate = FieldTruncate.None;
            Align = FieldAlign.Left;
        }
    }
}
