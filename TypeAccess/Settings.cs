﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TypeAccess
{
    public abstract class Settings
    {
        bool MaxSet;
        bool MinSet;
        public int MaxLength { get; protected set; }
        public FieldTruncate Truncate { get; protected set; }
        public int MinLength { get; protected set; }
        public FieldAlign Align { get; protected set; }

        protected void Maximum(int length, FieldTruncate truncate = FieldTruncate.None)    // Max Length behavior
        {
            MaxLength = length;
            Truncate = truncate;

            MaxSet = true;
        }

        protected void Minimum(int length, FieldAlign align = FieldAlign.Left)  // Minimum length behavior.
        {
            MinLength = length;
            Align = align;

            MinSet = true;
        }

        public IEnumerable<IHandler> GetHandlers()
        {
            if (MaxSet)
                yield return new LengthManagment(MaxLength, Truncate);

            if (MinSet)
                yield return new AlignmentManagment(MinLength, Align);
        }
    }

    // Max required Min optional
    public class SettingLengthKnown : Settings
    {
        private int _knownLength;

        public SettingLengthKnown(int knownLength)
        {
            _knownLength = knownLength;

            Maximum(_knownLength, FieldTruncate.None);
        }

        public void Maximum(FieldTruncate truncate = FieldTruncate.None)    // Max Length behavior
        {
            Maximum(_knownLength, truncate);
        }

        public void Minimum(FieldAlign align = FieldAlign.Left)  // Minimum length behavior.
        {
            Minimum(_knownLength, align);
        }
    }

    // Max & Min are required
    // when used with fixed length both min and max must have a value
    public class SettingLengthKnownFixed : Settings
    {
        private int _knownLength;

        public SettingLengthKnownFixed(int knownLength)
        {
            _knownLength = knownLength;

            Maximum(_knownLength, FieldTruncate.None);
            Minimum(_knownLength, FieldAlign.Left);
        }

        public void Maximum(FieldTruncate truncate = FieldTruncate.None)    // Max Length behavior
        {
            Maximum(_knownLength, truncate);
        }

        public void Minimum(FieldAlign align = FieldAlign.Left)  // Minimum length behavior.
        {
            Minimum(_knownLength, align);
        }
    }
}
