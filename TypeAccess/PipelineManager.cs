﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace TypeAccess
{
    public class PipelineManager    //this guy can know about types, like Repo
    {
        private readonly IDescription[] _descriptions;

        public PipelineManager(IEnumerable<IDescription> descriptions)
        {
            _descriptions = descriptions.ToArray();
        }

        public void GetString(object record, IRecordWriter writer)
        {
            writer.WriteStartRecord();

            for (int i = 0; i < _descriptions.Length; i++)
            {
                IDescription description = _descriptions[i];

                Nugget nugget = new Nugget(description);    // nugget dosent need whole description

                string value = nugget.GetString(record);

                description.Write(writer, value);
            }

            writer.WriteEndRecord();
        }

        public void GetObject(object record, IRecordReader reader)
        {
            reader.ReadRecord();

            for (int i = 0; i < _descriptions.Length; i++)
            {
                IDescription description = _descriptions[i];

                string value = description.Read(reader);

                Nugget nugget = new Nugget(description);    // nugget dosent need whole description

                nugget.GetObject(record, value);
            }
        }
    }

    public class Nugget
    {
        //private readonly IDescription _description;
        private Func<object, string> _stringify;
        private Action<string, object> _objectify;

        private PropertyInfo _property;
        private object _access;

        public Nugget(IDescription description)
        {
            _property = description.Property;
            _access = description.Access;
        }

        public Nugget(PropertyInfo property, object access)
        {
            _property = property;
            _access = access;
        }

        public string GetString(object record)
        {
            if (_stringify == null)
                _stringify = BuildStringify();

            return _stringify(record);
        }

        public void GetObject(object record, string value)
        {
            if (_objectify == null)
                _objectify = BuildObjectify();

            _objectify(value, record);
        }

        private object GetFieldHandler()
        {
            return _access;
        }

        private Func<object, string> BuildStringify()
        {
            DynamicMethod dynamicMethod = new DynamicMethod("Foo", typeof(string), new Type[] { this.GetType(), typeof(object) }, this.GetType(), true);

            var getFieldHandlerMethod = this.GetType().GetMethod("GetFieldHandler", BindingFlags.Instance | BindingFlags.NonPublic);

            ILGenerator il = dynamicMethod.GetILGenerator();
            //var @string = il.DeclareLocal(typeof(string));

            MethodInfo getPropertyMethod = _property.GetGetMethod();
            Type propertyType = getPropertyMethod.ReturnType;

            Type interf = typeof(IPropertyAccess<>).MakeGenericType(propertyType);
            MethodInfo conversionMethod = _access.GetType().GetInterfaceMap(interf).InterfaceMethods.Single(m => m.Name == "GetString");

            // -- Get FieldHandler --
            // push method argument 0 (this)
            il.Emit(OpCodes.Ldarg_0);

            // call method to get FieldHandler, pop 1, push handler
            il.Emit(OpCodes.Call, getFieldHandlerMethod);

            // -- Get poperty value --
            // push method argument 1 (T record)
            il.Emit(OpCodes.Ldarg_1);

            // call get property method, pop 1, push property value
            il.Emit(OpCodes.Call, getPropertyMethod);

            // call convert to string method, pop 2, push result
            il.Emit(OpCodes.Call, conversionMethod);

            // return
            il.Emit(OpCodes.Ret);

            return (Func<object, string>)dynamicMethod.CreateDelegate(typeof(Func<object, string>), this);
        }


        private Action<string, object> BuildObjectify()
        {
            string methodName = String.Format("{0}+Objectify2", typeof(RecordAccess).FullName);
            DynamicMethod dynamicMethod = new DynamicMethod(methodName, null, new Type[] { this.GetType(), typeof(string), typeof(object) }, this.GetType(), true);

            var getFieldHandlerMethod = this.GetType().GetMethod("GetFieldHandler", BindingFlags.Instance | BindingFlags.NonPublic);
            //var propertyExceptionMethod = typeof(RecordAccess).GetMethod("PropertyException", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Static);

            ILGenerator il = dynamicMethod.GetILGenerator();
            //var propertyValue = il.DeclareLocal(typeof(string));

            MethodInfo setPropertyMethod = _property.GetSetMethod();
            Type propertyType = setPropertyMethod.GetParameters()[0].ParameterType;

            Type interf = typeof(IPropertyAccess<>).MakeGenericType(propertyType);
            MethodInfo conversionMethod = _access.GetType().GetInterfaceMap(interf).InterfaceMethods.Single(m => m.Name == "GetValue");




            //il.BeginExceptionBlock();


            // push method argument 2 (T)
            il.Emit(OpCodes.Ldarg_2);


            // -- Get FieldHandler --
            // push method argument 0 (this)
            il.Emit(OpCodes.Ldarg_0);

            // push property index
            //il.Emit(OpCodes.Ldc_I4, i);

            // call method to get FieldHandler, pop 1, push handler
            il.Emit(OpCodes.Call, getFieldHandlerMethod);


            // -- Get array value --
            // push method argument 1 (string)
            il.Emit(OpCodes.Ldarg_1);

            // push property index 
            //il.Emit(OpCodes.Ldc_I4, i);

            // push array value, pop 2
            //il.Emit(OpCodes.Ldelem_Ref);

            // store property value in a local variable, pop 1
            //il.Emit(OpCodes.Stloc, propertyValue);

            // push property value
            //il.Emit(OpCodes.Ldloc, propertyValue);


            // -- Assign property value --
            // call convert from string method, pop 2, push result
            il.Emit(OpCodes.Call, conversionMethod);

            // call set property method, pop 2
            il.Emit(OpCodes.Call, setPropertyMethod);


            // -- Throw helpful exception --
            // push exception
            //il.BeginCatchBlock(typeof(Exception));

            // push property name
            //il.Emit(OpCodes.Ldstr, _descriptions[i].Property.Name);

            // push property value
            //il.Emit(OpCodes.Ldloc, propertyValue);

            // call throw exception method, pop 3
            //il.Emit(OpCodes.Call, propertyExceptionMethod);


            //il.EndExceptionBlock();


            // return
            il.Emit(OpCodes.Ret);

            return (Action<string, object>)dynamicMethod.CreateDelegate(typeof(Action<string, object>), this);
        }
    }
}
