﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace TypeAccess
{
    // we may want this to hold all state for a single type, so we dont have to look it up in repo everytime a property is accessed :(. Or we pass in the descriptions to the -ify method
    // to weant to use interface so that the way stringify and objectify can be replaced?

    /// <summary>
    /// Provides non type-safe access to an object
    /// </summary>
    public class RecordAccess
    {
        private readonly IDescription[] _descriptions;

        Func<object, string[]> _stringify;
        Action<string[], object> _objectify;


        public RecordAccess(IEnumerable<IDescription> descriptions)
        {
            _descriptions = descriptions.ToArray();
        }



        public string[] Stringify(object record)
        {
            if(_stringify == null)
                _stringify = CreateStringifyMethod<object>();

            return _stringify(record);
        }

        public void Objectify(object record, string[] values)
        {
            if (_objectify == null)
                _objectify = CreateObjectifyMethod2<object>();

            _objectify(values, record);
        }


        private object GetFieldHandler(int index)
        {
            return _descriptions[index].Access;
        }
                
        private static void PropertyException(Exception exception, string propertyName, string value)
        {
            throw new ApplicationException(String.Format("Value '{0}' is invalid for property {1}.", value, propertyName), exception);
        }

        public Func<T, string[]> CreateStringifyMethod<T>() //T is always object
        {
            string methodName = String.Format("{0}+Stringify", typeof(RecordAccess).FullName);
            DynamicMethod dynamicMethod = new DynamicMethod(methodName, typeof(string[]), new Type[] { typeof(RecordAccess), typeof(T) }, typeof(RecordAccess), true);

            var getFieldHandlerMethod = typeof(RecordAccess).GetMethod("GetFieldHandler", BindingFlags.Instance | BindingFlags.NonPublic);

            ILGenerator il = dynamicMethod.GetILGenerator();
            var array = il.DeclareLocal(typeof(string[]));

            // push array's size onto the stack 
            il.Emit(OpCodes.Ldc_I4, _descriptions.Length);

            // create new array, pop 1, push array
            il.Emit(OpCodes.Newarr, typeof(string));

            // store array in a local variable, pop 1
            il.Emit(OpCodes.Stloc, array);

            for (int i = 0; i < _descriptions.Length; i++)
            {
                MethodInfo getPropertyMethod = _descriptions[i].Property.GetGetMethod();
                Type propertyType = getPropertyMethod.ReturnType;
                //MethodInfo conversionMethod = _descriptions[i].Access.GetType().GetMethod("GetString", new Type[] { _descriptions[i].Property.PropertyType });

                Type interf = typeof(IPropertyAccess<>).MakeGenericType(propertyType);

                MethodInfo conversionMethod = _descriptions[i].Access.GetType().GetInterfaceMap(interf).InterfaceMethods.Single(m => m.Name == "GetString");//.GetMethods(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);


                
                // push the array
                il.Emit(OpCodes.Ldloc, array);

                // push property index 
                il.Emit(OpCodes.Ldc_I4, i);


                // -- Get FieldHandler --
                // push method argument 0 (this)
                il.Emit(OpCodes.Ldarg_0);

                // push property index 
                il.Emit(OpCodes.Ldc_I4, i);

                // call method to get FieldHandler, pop 2, push handler
                il.Emit(OpCodes.Call, getFieldHandlerMethod);

                
                // -- Get poperty value --
                // push method argument 1 (T record)
                il.Emit(OpCodes.Ldarg_1);

                // call get property method, pop 1, push property value
                il.Emit(OpCodes.Call, getPropertyMethod);

                // call convert to string method, pop 2, push result
                il.Emit(OpCodes.Call, conversionMethod);                

                // add property value as string to the array, pop 3
                il.Emit(OpCodes.Stelem_Ref);
            }

            // push array
            il.Emit(OpCodes.Ldloc, array);

            // return
            il.Emit(OpCodes.Ret);

            return (Func<T, string[]>)dynamicMethod.CreateDelegate(typeof(Func<T, string[]>), this);
        }

        public Action<string[], T> CreateObjectifyMethod2<T>()  //T is always object // todo: remove T when we know that we can
        {
            string methodName = String.Format("{0}+Objectify2", typeof(RecordAccess).FullName);
            DynamicMethod dynamicMethod = new DynamicMethod(methodName, null, new Type[] { typeof(RecordAccess), typeof(string[]), typeof(T) }, typeof(RecordAccess), true);

            var getFieldHandlerMethod = typeof(RecordAccess).GetMethod("GetFieldHandler", BindingFlags.Instance | BindingFlags.NonPublic);
            var propertyExceptionMethod = typeof(RecordAccess).GetMethod("PropertyException", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Static);

            ILGenerator il = dynamicMethod.GetILGenerator();
            var propertyValue = il.DeclareLocal(typeof(string));


            for (int i = 0; i < _descriptions.Length; i++)
            {
                MethodInfo setPropertyMethod = _descriptions[i].Property.GetSetMethod();
                Type propertyType = setPropertyMethod.GetParameters()[0].ParameterType;
                //MethodInfo conversionMethod = _descriptions[i].Access.GetType().GetMethod("GetValue", BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);  //todo: restrivt to return type

                


                Type unboundGenericList = typeof(IPropertyAccess<>);
                Type interf = unboundGenericList.MakeGenericType(propertyType);

                MethodInfo conversionMethod = _descriptions[i].Access.GetType().GetInterfaceMap(interf).InterfaceMethods.Single(m => m.Name == "GetValue");//.GetMethods(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);



                il.BeginExceptionBlock();


                // push method argument 2 (T)
                il.Emit(OpCodes.Ldarg_2);


                // -- Get FieldHandler --
                // push method argument 0 (this)
                il.Emit(OpCodes.Ldarg_0);

                // push property index
                il.Emit(OpCodes.Ldc_I4, i);

                // call method to get FieldHandler, pop 2, push handler
                il.Emit(OpCodes.Call, getFieldHandlerMethod);


                // -- Get array value --
                // push method argument 1 (string[])
                il.Emit(OpCodes.Ldarg_1);

                // push property index 
                il.Emit(OpCodes.Ldc_I4, i);

                // push array value, pop 2
                il.Emit(OpCodes.Ldelem_Ref);

                // store property value in a local variable, pop 1
                il.Emit(OpCodes.Stloc, propertyValue);

                // push property value
                il.Emit(OpCodes.Ldloc, propertyValue);


                // -- Assign property value --
                // call convert from string method, pop 2, push result
                il.Emit(OpCodes.Callvirt, conversionMethod);

                // call set property method, pop 2
                il.Emit(OpCodes.Callvirt, setPropertyMethod);


                // -- Throw helpful exception --
                // push exception
                il.BeginCatchBlock(typeof(Exception));

                // push property name
                il.Emit(OpCodes.Ldstr, _descriptions[i].Property.Name);

                // push property value
                il.Emit(OpCodes.Ldloc, propertyValue);

                // call throw exception method, pop 3
                il.Emit(OpCodes.Call, propertyExceptionMethod);
                

                il.EndExceptionBlock();
            }

            // return
            il.Emit(OpCodes.Ret);

            return (Action<string[], T>)dynamicMethod.CreateDelegate(typeof(Action<string[], T>), this);
        }
    }
}
