﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TypeAccess
{
    public interface IPropertyAccess<T>
    {
        string GetString(T value);

        T GetValue(string value);
    }
}
