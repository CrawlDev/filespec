﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TypeAccess
{
    public class RecordWriter : IRecordWriter
    {
        private readonly TextWriter _writer;
        private readonly string _fieldDelimiter;
        private readonly string _recordDelimiter;

        private List<StringBuilder> _buffers;

        public RecordWriter(TextWriter writer, string fieldDelimiter = ",", string recordDelimiter = "\r\n")
        {
            _writer = writer;
            _fieldDelimiter = fieldDelimiter;
            _recordDelimiter = recordDelimiter;

            _buffers = new List<StringBuilder>();
        }

        public void WriteStartRecord()
        {
            ClearBuffers();
        }

        public void Write(string value, int delimitedContext)
        {
            StringBuilder buffer = GetBuffer(delimitedContext);
            buffer.Append(value);
        }

        public void Write(string value, int delimitedContext, int index)
        {
            StringBuilder buffer = GetBuffer(delimitedContext);

            if (buffer.Length < index)
                buffer.Append(' ', index - buffer.Length);
            else if (buffer.Length > index)
                throw new ApplicationException("Cant write value at specified index."); //shouldn't happen if we write in index order

            buffer.Append(value);
        }

        public void WriteEndRecord()
        {
            WriteBuffers(_writer);
        }

        public void WriteEndRecord(TextWriter writer)   // hacked in
        {
            WriteBuffers(writer);
        }

        private void ClearBuffers()
        {
            foreach (StringBuilder buffer in _buffers)
                buffer.Clear();
        }

        private void WriteBuffers(TextWriter writer)
        {
            bool hasData = false;

            foreach (StringBuilder buffer in _buffers)
            {
                if (hasData)
                    writer.Write(_fieldDelimiter);

                writer.Write(buffer);

                hasData = true;
            }

            writer.Write(_recordDelimiter);
        }

        private StringBuilder GetBuffer(int delimitedContext)
        {
            while (_buffers.Count <= delimitedContext)
                _buffers.Add(new StringBuilder());

            return _buffers[delimitedContext];
        }
    }


    public class RecordWriterFixed : IRecordWriter
    {
        private readonly TextWriter _writer;
        private readonly string _recordDelimiter;

        private StringBuilder _buffer;

        public RecordWriterFixed(TextWriter writer, string recordDelimiter = "\r\n")
        {
            _writer = writer;
            _recordDelimiter = recordDelimiter;

            _buffer = new StringBuilder();
        }

        public void WriteStartRecord()
        {
            _buffer.Clear();
        }

        public void Write(string value, int delimitedContext)
        {
            if (delimitedContext != 0)
                throw new ArgumentOutOfRangeException("delimitedContext");

            _buffer.Append(value);
        }

        public void Write(string value, int delimitedContext, int index)
        {
            if (delimitedContext != 0)
                throw new ArgumentOutOfRangeException("delimitedContext");

            if (_buffer.Length < index)
                _buffer.Append(' ', index - _buffer.Length);
            else if (_buffer.Length > index)
                throw new ApplicationException("Cant write value at specified index."); //shouldn't happen if we write in index order

            _buffer.Append(value);
        }

        public void WriteEndRecord(TextWriter writer)   // hacked in
        {
            writer.Write(_buffer);
            writer.Write(_recordDelimiter);
        }

        public void WriteEndRecord()
        {
            _writer.Write(_buffer);
            _writer.Write(_recordDelimiter);
        }
    }


    public interface IRecordWriter
    {
        void Write(string value, int delimitedContext);
        void Write(string value, int delimitedContext, int index);
        void WriteEndRecord();
        void WriteEndRecord(TextWriter writer);
        void WriteStartRecord();
    }
}
