﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TypeAccess
{
    public interface IRecordReader
    {
        int PartCount { get; }   //todo: rename

        string[] Current();
        void ReadRecord();
        bool ReadRecord(TextReader reader);

        string Fetch(int delimitedContext);
        string Fetch(int delimitedContext, int index);
        string Fetch(int delimitedContext, int index, int length);
    }

    public class RecordReader : IRecordReader
    {
        private readonly TextReader _reader;
        private readonly string _fieldDelimiter;
        private readonly string _recordDelimiter;

        private string[] _parts;
        private int _i;

        public int PartCount    //todo: rename
        {
            get { return _parts.Length; }
        }

        public RecordReader(TextReader reader, string fieldDelimiter = ",", string recordDelimiter = "\r\n")
        {
            _reader = reader;
            _fieldDelimiter = fieldDelimiter;
            _recordDelimiter = recordDelimiter;
        }



        public void ReadRecord()
        {
            ReadRecord(_reader);    //hacked in
        }

        public bool ReadRecord(TextReader reader)
        {
            string line = reader.ReadLine();    //todo: implement record delimiter

            if (line == null)
                return false;

            _parts = line.Split(new string[] { _fieldDelimiter }, StringSplitOptions.None);   //todo: dont split, read char by char
            _i = 0;

            return true;
        }

        //public string Read()
        //{
        //    return _parts[_i++];
        //}

        public string[] Current()
        {
            return _parts;
        }

        // only thing diffenrt between this and the fixed length one is how the delimited context is resolved.
        public string Fetch(int delimitedContext)
        {
            return _parts[delimitedContext];
        }

        public string Fetch(int delimitedContext, int index)
        {
            return _parts[delimitedContext].Substring(index);
        }

        public string Fetch(int delimitedContext, int index, int length)
        {
            return _parts[delimitedContext].Substring(index, length);
        }
    }


    // maybe this guy just always gives the same string when asked for read, the other reader will return the string from parts[]..
    // nah this makes the description tied to a particular reader offset would have diffent meanings.
    // on second thought, this may be the best option
    public class RecordReaderFixed : IRecordReader
    {
        private readonly TextReader _reader;
        private readonly string _recordDelimiter;

        public int PartCount    //todo: rename
        {
            get { return 1; }
        }

        public RecordReaderFixed(TextReader reader, string recordDelimiter = "\r\n")
        {
            _reader = reader;
            _recordDelimiter = recordDelimiter;
        }

        private string _line;
        private int _index;

        public void ReadRecord()
        {
            ReadRecord(_reader);
        }

        public bool ReadRecord(TextReader reader)
        {
            _line = reader.ReadLine();  //todo: implement record delimiter

            if (_line == null)
                return false;

            _index = 0;

            return true;
        }

        //public string Read(int length)
        public string Read() // should be "gimmie delimited context for field x"
        {
            return _line;
        }

        public string[] Current()
        {
            return new string[] { _line };
        }

        public string Fetch(int delimitedContext)
        {
            if (delimitedContext != 0)
                throw new ArgumentOutOfRangeException("delimitedContext");

            return _line;
        }

        public string Fetch(int delimitedContext, int index)
        {
            if (delimitedContext != 0)
                throw new ArgumentOutOfRangeException("delimitedContext");

            return _line.Substring(index);
        }

        public string Fetch(int delimitedContext, int index, int length)
        {
            if (delimitedContext != 0)
                throw new ArgumentOutOfRangeException("delimitedContext");

            //todo: length >= 0;

            return _line.Substring(index, length);
        }


    }
}
