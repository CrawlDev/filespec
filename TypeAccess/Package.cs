﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TypeAccess
{
    public class Package
    {
        public List<IDescription2> Descriptions;
        public IRecordWriter Writer;
        public IRecordReader Reader;
        public Func<string, bool> Predicate;
        public Func<object> Create;

        public void Write(object record, TextWriter writer)
        {
            Writer.WriteStartRecord();

            foreach (IDescription2 description in Descriptions)
                description.Write(record, Writer);

            Writer.WriteEndRecord(writer);
        }

        public bool Read(object record, TextReader reader)
        {
            bool read = Reader.ReadRecord(reader);

            if (read)
            {
                foreach (IDescription2 description in Descriptions)
                    description.Read(record, Reader);

                return true;
            }

            return false;
        }
    }
}
