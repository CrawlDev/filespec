﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TypeAccess
{
    public class TimeSpanAccess : IPropertyAccess<TimeSpan>
    {
        private string _format;

        public TimeSpanAccess(string format = null)
        {
            _format = format;
        }

        public string GetString(TimeSpan value)
        {
            return value.ToString(_format);
        }

        public TimeSpan GetValue(string text)
        {
            return TimeSpan.ParseExact(text, _format, null);
        }
    }
}
