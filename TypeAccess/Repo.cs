﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TypeAccess
{
    /// <summary>
    /// Type=safe access to an object
    /// </summary>
    public class Repo
    {
        private readonly Dictionary<Type, List<IDescription>> _lookup;   //todo: spit the class based on these 2 reponsibilities
        //private readonly Dictionary<Type, RecordAccess> _access;

        public Repo()
        {
            _lookup = new Dictionary<Type, List<IDescription>>();
            //_access = new Dictionary<Type, RecordAccess>();
        }

        public Builder<T> Register<T>()
        {
            return new Builder<T>(this);        // handing the builder out like this we never no when its gonna change. Oh we can invalidate _access on Add()
        }

        //public BuilderFromAttributes<T> RegisterAttributes<T>() //todo: consolidate this with the other builder? Extension assembly?
        //{
        //    return new BuilderFromAttributes<T>(this);
        //}

        internal void Add<T>(IDescription description)
        {
            Add(typeof(T), description);
        }

        internal void Add(Type type, IDescription description)
        {
            List<IDescription> list;

            if (!_lookup.TryGetValue(type, out list))
            {
                list = new List<IDescription>();
                _lookup.Add(type, list);
            }

            list.Add(description);
        }

        public int CountDescriptions<T>()
        {
            return CountDescriptions(typeof(T));
        }

        public int CountDescriptions(Type type)
        {
            List<IDescription> list;

            if (!_lookup.TryGetValue(type, out list))
                return 0;

            return list.Count();
        }

        public IEnumerable<IDescription> Get<T>()    //todo: private
        {
            return Get(typeof(T));
        }

        private IEnumerable<IDescription> Get(Type type)
        {
            return _lookup[type];   //todo: guard, null?
        }

        //public RecordAccess<T> GetAccess<T>()
        //{
        //    return GetAccess(typeof(T));
        //}


        // maybe keep RecordAccess internal?
        //private RecordAccess GetAccess<T>()
        //{
        //    Type type = typeof(T);
        //    RecordAccess recordAccess;

        //    if (!_access.TryGetValue(type, out recordAccess))
        //    {
        //        recordAccess = new RecordAccess(Get(type));

        //        _access.Add(type, recordAccess);
        //    }

        //    return recordAccess;

        //    //RecordAccess<T> recordAccess = Cache<T>.Instance;

        //    //if (recordAccess == null)
        //    //{
        //    //    recordAccess = new RecordAccess<T>();
        //    //    recordAccess.Load(Get(typeof(T)));

        //    //    Cache<T>.Instance = recordAccess;
        //    //}

        //    //return recordAccess;

        //    //return null;
        //}


        //public string[] Stringify<T>(T record)
        //{
        //    RecordAccess recordAccess = GetAccess<T>();
        //    return recordAccess.Stringify(record);
        //}

        //public void Objectify<T>(T record, string[] values)
        //{
        //    RecordAccess recordAccess = GetAccess<T>();
        //    recordAccess.Objectify(record, values);
        //}
    }
}
