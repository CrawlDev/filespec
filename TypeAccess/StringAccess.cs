﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TypeAccess
{
    //todo: how about UpperCase? No. Otherthings would like uppercase as well (certain date formats)
    public class StringAccess : IPropertyAccess<string>
    {
        public string GetString(string value)
        {
            return value;
        }

        public string GetValue(string value)
        {
            return value;
        }
    }
}
