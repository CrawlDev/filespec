﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TypeAccess
{
    public class Bai2RecordReader
    {
        private readonly MyReader _reader;
        private readonly StringBuilder _buffer;
        private readonly List<string> _fields;

        private char[] _chars;
        private bool _recordComplete;
        private int _fieldIndex;
        private int _textFieldIndex;

        public List<string> Fields
        {
            get { return _fields; }
        }

        public Bai2RecordReader(Stream stream)
        {
            _reader = new MyReader(stream);
            _buffer = new StringBuilder();
            _fields = new List<string>();

            _chars = new char[1];

            _reader.Read(_chars, 0, 1);
        }

        public bool ReadRecord()
        {
            _fieldIndex = 0;
            _textFieldIndex = -1;
            _recordComplete = false;
            _fields.Clear();

            while (!_reader.EndOfStream)
            {
                _buffer.Clear();

                if (_fieldIndex == _textFieldIndex)
                {
                    ReadText();
                }
                else
                {
                    ReadNonText();
                    UpdateFieldState();
                }

                _fields.Add(_buffer.ToString());
                _fieldIndex++;

                if (_recordComplete)
                    return true;
            }

            return false;
        }

        private void ReadNonText()
        {
            while (!_reader.EndOfStream)
            {
                if (_chars[0] == ',')
                {
                    _reader.Read(_chars, 0, 1);
                    return;
                }
                else if (_chars[0] == '/')
                {
                    _reader.Read(_chars, 0, 1);

                    ReadWhitespace();

                    if (!PeekContinuation())
                        _recordComplete = true;

                    return;
                }
                else
                {
                    _buffer.Append(_chars[0]);
                    _reader.Read(_chars, 0, 1);
                }
            }
        }

        private void ReadText()
        {
            while (!_reader.EndOfStream)
            {
                if (_buffer.Length == 0 && _chars[0] == '/')    // first char
                {
                    _reader.Read(_chars, 0, 1);

                    ReadWhitespace();

                    _recordComplete = true;
                    return;
                }
                else if (_chars[0] == '\r')
                {
                    _reader.Read(_chars, 0, 1);

                    if (_chars[0] == '\n')
                        _reader.Read(_chars, 0, 1);

                    if (!PeekContinuation())
                    {
                        _recordComplete = true;
                        return;
                    }
                }
                else if (_chars[0] == '\n')
                {
                    _reader.Read(_chars, 0, 1);

                    if (!PeekContinuation())
                    {
                        _recordComplete = true;
                        return;
                    }
                }
                else
                {
                    _buffer.Append(_chars[0]);
                    _reader.Read(_chars, 0, 1);
                }
            }

            _recordComplete = true;  // EOF delimits
        }

        private bool PeekContinuation()
        {
            if (_chars[0] == '8')
            {
                char[] peek = new char[2];

                if (_reader.Peek(peek, 0, 2) != 2)
                    return false;

                if (!peek.SequenceEqual("8,"))
                    return false;

                _reader.Accept();

                _reader.Read(_chars, 0, 1);

                return true;
            }

            return false;
        }


        private void ReadWhitespace()
        {
            while (!_reader.EndOfStream)
            {
                if (!IsWhiteSpace(_chars[0]))
                    break;

                _reader.Read(_chars, 0, 1);
            }
        }

        private void UpdateFieldState()
        {
            if (_fieldIndex == 4 && _fields[0] == "16")
            {
                switch (_fields[3])
                {
                    case "S":
                        _textFieldIndex = 6 + 3;
                        break;

                    case "V":
                        _textFieldIndex = 6 + 2;
                        break;

                    case "D":
                        int n = Int32.Parse(_fields[4]);
                        _textFieldIndex = 6 + 1 + (n * 2);
                        break;

                    default:
                        _textFieldIndex = 6;
                        break;
                }
            }
        }

        private bool IsWhiteSpace(char c)
        {
            return c == '\r' || c == '\n' || c == ' ';
        }
    }



    public class MyReader
    {
        private BinaryReader _reader;
        private StringBuilder _buffer;
        private int _bufferIndex = 0;

        private bool _endOfStream;

        public bool EndOfStream
        {
            get { return _endOfStream; }
        }

        public MyReader(Stream stream)
        {
            _reader = new BinaryReader(stream);
            _buffer = new StringBuilder();
            _endOfStream = false;
        }

        public int Peek(char[] buffer, int index, int count)    // if we start another peek from where are reading, it wont be the next chars after the read.
        {
            var streamRead = _reader.Read(buffer, index, count);

            _buffer.Append(buffer, index, streamRead);

            return streamRead;
        }

        public void Accept()
        {
            _buffer.Clear();
        }

        public int Read(char[] buffer, int index, int count)
        {
            var bufferRead = Math.Min(_buffer.Length, count);

            _buffer.CopyTo(_bufferIndex, buffer, index, bufferRead);

            _bufferIndex += bufferRead;

            if (_bufferIndex == _buffer.Length)
            {
                _buffer.Clear();
                _bufferIndex = 0;
            }

            var streamRead = _reader.Read(buffer, index + bufferRead, count - bufferRead);

            if (bufferRead + streamRead < count)
                _endOfStream = true;

            return bufferRead + streamRead;
        }
    }
}
