﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TypeAccess
{
    public interface IDescription2
    {
        Nugget Nugget { get; }
        IField Field { get; set; }

        void Write(object record, IRecordWriter writer);
        void Read(object record, IRecordReader reader);
    }

    // I want this to be the thing that bridges the type acces and field access
    public class Description3 : IDescription2
    {
        public Nugget Nugget { get; set; }
        public IField Field { get; set; }

        public void Write(object record, IRecordWriter writer)
        {
            string value = Nugget.GetString(record);

            Field.Write(writer, value);
        }

        public void Read(object record, IRecordReader reader)
        {
            string value = Field.Read(reader);

            Nugget.GetObject(record, value);
        }
    }
}
