﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace TypeAccess
{    

    // Caching built-in
    public static class Instance
    {
        private static Dictionary<Type, Func<object>> _cache;

        static Instance()
        {
            _cache = new Dictionary<Type, Func<object>>();
        }

        public static object Of(Type type)
        {
            Func<object> f;

            if(!_cache.TryGetValue(type, out f))
            {
                f = BuildDelegate(type);
                _cache.Add(type, f);
            }

            return f();
        }

        private static Func<object> BuildDelegate(Type type)
        {
            DynamicMethod method = new DynamicMethod("Create", type, null, typeof(Instance), true);
            ILGenerator il = method.GetILGenerator();

            il.Emit(OpCodes.Newobj, type.GetConstructor(Type.EmptyTypes));
            il.Emit(OpCodes.Ret);

            return (Func<object>)method.CreateDelegate(typeof(Func<object>));
        }
    }

    public static class Instance<T>
    {
        private static Func<T> _f;

        static Instance()
        {
            _f = BuildDelegate();
        }

        public static object Of()
        {
            return _f();
        }

        private static Func<T> BuildDelegate()
        {
            Type type = typeof(T);

            DynamicMethod method = new DynamicMethod("Create", type, null, typeof(Instance<T>), true);
            ILGenerator il = method.GetILGenerator();

            il.Emit(OpCodes.Newobj, type.GetConstructor(Type.EmptyTypes));
            il.Emit(OpCodes.Ret);

            return (Func<T>)method.CreateDelegate(typeof(Func<T>));
        }
    }

    public class Instance2
    {
        private Func<object> _f;

        public Instance2(Type type)
        {
            _f = BuildDelegate(type);
        }

        public object Of()
        { 
            return _f();
        }

        private Func<object> BuildDelegate(Type type)
        {
            DynamicMethod method = new DynamicMethod("Create", type, null, typeof(Instance2), true);
            ILGenerator il = method.GetILGenerator();

            il.Emit(OpCodes.Newobj, type.GetConstructor(Type.EmptyTypes));
            il.Emit(OpCodes.Ret);

            return (Func<object>)method.CreateDelegate(typeof(Func<object>));
        }
    }
}
