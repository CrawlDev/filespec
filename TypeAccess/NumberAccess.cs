﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TypeAccess
{
    public class NumberAccess : IPropertyAccess<byte>, IPropertyAccess<short>, IPropertyAccess<int>, IPropertyAccess<long>, IPropertyAccess<decimal>
    {
        private readonly string _format;
        private readonly int _scale;

        public NumberAccess(string format = null, int scale = 1)
        {
            _format = format;
            _scale = scale;
        }

        public string GetString(byte value)
        {
            string result;

            value = (byte)(value * _scale);
            result = value.ToString(_format);

            return result;
        }

        byte IPropertyAccess<byte>.GetValue(string text)
        {
            byte result = 0;

            if (Byte.TryParse(text, out result))
                result = (byte)(result / _scale);

            return result;
        }

        public string GetString(short value)
        {
            string result;

            value *= (short)(_scale);
            result = value.ToString(_format);

            return result;
        }

        short IPropertyAccess<short>.GetValue(string text)
        {
            short result = 0;

            if (Int16.TryParse(text, out result))
                result = (short)(result / _scale);

            return result;
        }

        public string GetString(int value)
        {
            string result;

            value *= _scale;
            result = value.ToString(_format);

            return result;
        }

        int IPropertyAccess<int>.GetValue(string text)
        {
            int result = 0;

            if (Int32.TryParse(text, out result))
                result = (int)(result / _scale);

            return result;
        }

        public string GetString(long value)
        {
            string result;

            value = (long)(value * _scale);
            result = value.ToString(_format);

            return result;
        }

        long IPropertyAccess<long>.GetValue(string text)
        {
            long result = 0;

            if (Int64.TryParse(text, out result))
                result = (long)(result / _scale);

            return result;
        }

        public string GetString(decimal value)
        {
            string result;

            value *= _scale;
            result = value.ToString(_format);

            return result;
        }

        decimal IPropertyAccess<decimal>.GetValue(string text)
        {
            decimal result = 0;

            if (Decimal.TryParse(text, out result))
                result = (decimal)(result / _scale);

            return result;
        }


    }
}
