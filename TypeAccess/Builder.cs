﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace TypeAccess
{
    public class Builder<T>
    {
        private readonly Repo _repo;

        public Builder(Repo repo)
        {
            _repo = repo;
        }

        public Builder<T> Property<R>(Expression<Func<T, R>> property, IPropertyAccess<R> access)
        {
            //_descriptions.Add(new Description { Property = GetProperty(property), Access = access });
            _repo.Add<T>(new Description { Position = _repo.CountDescriptions<T>(), Property = GetProperty(property), Access = access });

            return this;
        }

        public Builder<T> Property<R>(Expression<Func<T, R>> property, IPropertyAccess<R> access, params IHandler[] chain)
        {
            _repo.Add<T>(new Description { Position = _repo.CountDescriptions<T>(), Property = GetProperty(property), Access = access, Handlers = chain });

            return this;
        }


        //public Builder<T> Property<R>(Expression<Func<T, R>> property, IPropertyAccess<R> access, Action<Settings> a)
        //{
        //    var settings = new Settings();
        //    a(settings);

        //    var h = settings.GetHandlers();

        //    _repo.Add<T>(new Description { Position = _repo.CountDescriptions<T>(), Property = GetProperty(property), Access = access, Handlers = h.ToArray() });

        //    return this;
        //}

        public Builder<T> Property<R>(Expression<Func<T, R>> property, int length, IPropertyAccess<R> access, Action<SettingLengthKnown> a)
        {
            var settings = new SettingLengthKnown(length);
            a(settings);

            var h = settings.GetHandlers();

            _repo.Add<T>(new Description { Position = _repo.CountDescriptions<T>(), Property = GetProperty(property), Access = access, Handlers = h.ToArray() });

            return this;
        }

        public Builder<T> Property(Expression<Func<T, int>> property)
        {
            //_descriptions.Add(new Description { Property = GetProperty(property), Access = new NumberAccess() });
            _repo.Add<T>(new Description { Position = _repo.CountDescriptions<T>(), Property = GetProperty(property), Access = new NumberAccess() });

            return this;
        }

        public Builder<T> Property<R>(Expression<Func<T, R>> property)
        {
            //_descriptions.Add(new Description { Property = GetProperty(property), Access = new GenericAccess4<R>() });
            _repo.Add<T>(new Description { Position = _repo.CountDescriptions<T>(), Property = GetProperty(property), Access = new GenericAccess4<R>() });

            return this;
        }

        private PropertyInfo GetProperty<R>(Expression<Func<T, R>> propertyExpression)
        {
            var memberExpression = propertyExpression.Body as MemberExpression;

            return memberExpression.Member as PropertyInfo;
        }
    }

    /*
        public class BuilderFromAttributes<T>
        {
            //public List<Description> _descriptions = new List<Description>();   // todo: protect this

            private readonly Repo _repo;

            public BuilderFromAttributes(Repo repo)
            {
                _repo = repo;
            }

            public void Go()
            {
                var typeInfo = typeof(T).GetProperties(BindingFlags.Instance | BindingFlags.Public)
                     .Select(p => new { Property = p, Attribute = (FieldAttribute)p.GetCustomAttributes(typeof(FieldAttribute), true).FirstOrDefault() })
                     .Where(f => f.Attribute != null)
                     .OrderBy(f => f.Attribute.Index)
                     .ToArray();

                foreach (var ti in typeInfo)
                {
                    Type propertyType = ti.Property.PropertyType;

                    TextFieldAttribute textAttribute = ti.Attribute as TextFieldAttribute;
                    if (textAttribute != null)
                    {
                        if (IsNumeric(propertyType))
                        {
                            _repo.Add<T>(new Description { Position = _repo.CountDescriptions<T>(), Property = ti.Property, Access = new NumberAccess(textAttribute.Format, textAttribute.Scale) });
                        }
                        else if (propertyType == typeof(bool))
                        {
                            _repo.Add<T>(new Description { Position = _repo.CountDescriptions<T>(), Property = ti.Property, Access = new BooleanAccess() });
                        }
                        else if (propertyType == typeof(DateTime))
                        {
                            _repo.Add<T>(new Description { Position = _repo.CountDescriptions<T>(), Property = ti.Property, Access = new DateTimeAccess(textAttribute.Format) });
                        }
                        else if (propertyType == typeof(TimeSpan))
                        {
                            _repo.Add<T>(new Description { Position = _repo.CountDescriptions<T>(), Property = ti.Property, Access = new TimeSpanAccess(textAttribute.Format) });
                        }
                        else if (propertyType == typeof(string))
                        {
                            _repo.Add<T>(new Description { Position = _repo.CountDescriptions<T>(), Property = ti.Property, Access = new StringAccess() });
                        }
                    }

                    //todo: other attribute types
                }
            }

            public void GoFixed()
            {
                var typeInfo = typeof(T).GetProperties(BindingFlags.Instance | BindingFlags.Public)
                     .Select(p => new { Property = p, Attribute = (FieldAttribute)p.GetCustomAttributes(typeof(FieldAttribute), true).FirstOrDefault() })
                     .Where(f => f.Attribute != null)
                     .OrderBy(f => f.Attribute.Index)
                     .ToArray();

                foreach (var ti in typeInfo)
                {
                    Type propertyType = ti.Property.PropertyType;

                    TextFieldAttribute textAttribute = ti.Attribute as TextFieldAttribute;
                    if (textAttribute != null)
                    {
                        if (IsNumeric(propertyType))
                        {
                            _repo.Add<T>(new Description2 { Index = textAttribute.Index, Length = textAttribute.Length, Property = ti.Property, Access = new NumberAccess(textAttribute.Format, textAttribute.Scale) });
                        }
                        else if (propertyType == typeof(bool))
                        {
                            _repo.Add<T>(new Description2 { Index = textAttribute.Index, Length = textAttribute.Length, Property = ti.Property, Access = new BooleanAccess() });
                        }
                        else if (propertyType == typeof(DateTime))
                        {
                            _repo.Add<T>(new Description2 { Index = textAttribute.Index, Length = textAttribute.Length, Property = ti.Property, Access = new DateTimeAccess(textAttribute.Format) });
                        }
                        else if (propertyType == typeof(TimeSpan))
                        {
                            _repo.Add<T>(new Description2 { Index = textAttribute.Index, Length = textAttribute.Length, Property = ti.Property, Access = new TimeSpanAccess(textAttribute.Format) });
                        }
                        else if (propertyType == typeof(string))
                        {
                            _repo.Add<T>(new Description2 { Index = textAttribute.Index, Length = textAttribute.Length, Property = ti.Property, Access = new StringAccess() });
                        }
                    }

                    //todo: other attribute types
                }

            }

            private bool IsNumeric(Type propertyType)
            {
                return propertyType == typeof(byte)
                    || propertyType == typeof(short)
                    || propertyType == typeof(ushort)
                    || propertyType == typeof(int)
                    || propertyType == typeof(uint)
                    || propertyType == typeof(long)
                    || propertyType == typeof(ulong)
                    || propertyType == typeof(decimal);
            }
        }
     */
}
