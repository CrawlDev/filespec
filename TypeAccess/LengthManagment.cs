﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TypeAccess
{
    public interface IHandler
    {
        string Write(string value);
        string Read(string value);
    }

    public class LengthManagment : IHandler
    {
        private readonly int _maxLength;
        private readonly FieldTruncate _truncate;

        public LengthManagment(int maxLength = -1, FieldTruncate truncate = FieldTruncate.None)
        {
            _maxLength = maxLength;
            _truncate = truncate;
        }

        public string Write(string value)
        {
            // Make sure the value will fit in the space allocated
            if (value != null && _maxLength > 0 && value.Length > _maxLength)
            {
                switch (_truncate)
                {
                    case FieldTruncate.Left:
                        value = value.Substring(value.Length - _maxLength);
                        break;

                    case FieldTruncate.Right:
                        value = value.Substring(0, _maxLength);
                        break;

                        //todo: how do we get the property name? catch exception outside?
                    case FieldTruncate.None:
                        throw new ApplicationException("Value is longer than space allocated.");
                        //throw new ApplicationException(String.Format("Value is longer than space allocated. The length of the value '{0}' exceeds the maximum field length of field '{1}'", value, field.PropertyName));
                }
            }

            return value;
        }

        public string Read(string value)
        {
            return value.Substring(0, _maxLength);
        }


        //public virtual string Deserialize(FieldDescriptor field, string value)
        //{
        //    string result = null;

        //    if (value != null && !String.IsNullOrWhiteSpace(value))
        //        result = value.Trim();

        //    return result;
        //}

        //public virtual string Serialize(FieldDescriptor field, string value)
        //{
        //    return Truncate(field, value);
        //}

        //private string Truncate(FieldDescriptor field, string value)
        //{
        //    // Make sure the value will fit in the space allocated
        //    if (value != null && field.Attribute.Length > 0 && value.Length > field.Attribute.Length)
        //    {
        //        switch (field.Attribute.Truncate)
        //        {
        //            case FieldTruncate.Left:
        //                value = value.Substring(value.Length - field.Attribute.Length);
        //                break;

        //            case FieldTruncate.Right:
        //                value = value.Substring(0, field.Attribute.Length);
        //                break;

        //            case FieldTruncate.None:
        //                throw new ApplicationException(String.Format("Value is longer than space allocated. The length of the value '{0}' exceeds the maximum field length of field '{1}'", value, field.PropertyName));
        //        }
        //    }

        //    return value;
        //}
    }


    // if we are padding, then that is really the min length that the fied must be.. or its just the defined width of the column
    public class AlignmentManagment : IHandler
    {
        private readonly int _maxLength;
        private readonly FieldAlign _align;

        public AlignmentManagment(int maxLength, FieldAlign align = FieldAlign.Left)
        {
            _maxLength = maxLength;
            _align = align;
        }

        public string Write(string value)
        {
            string format = String.Format("{{0,{0}{1}}}", (_align == FieldAlign.Left) ? "-" : "", _maxLength);  //todo: create this once or use PadLeft/PadRight

            return String.Format(format, value);
        }

        public string Read(string value)
        {
            return value;   // could do trim left/right
        }
    }

}
