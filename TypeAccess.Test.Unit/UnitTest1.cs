﻿using System;
using System.IO;
using System.Text;
using Xunit;

namespace TypeAccess.Test.Unit
{
    public class UnitTest1
    {
        private StringBuilder _buffer;
        private IRecordWriter _writer;

        [Fact]
        public void Write_PositionOnly_CorrectValueSentToWriter()
        {
            Field field = new Field(0);


            _writer.WriteStartRecord();

            string value = "ABCDEFGHIJ";

            field.Write(_writer, value);
            
            _writer.WriteEndRecord();

            Assert.Equal(value, _buffer.ToString());
        }

        // calls maximum behavior


        // calls writer with correct position


        private void Setup()
        {
            _buffer = new StringBuilder();
            _writer = new RecordWriter(new StringWriter(_buffer), recordDelimiter: "");
        }
    }


}
